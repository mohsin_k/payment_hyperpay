##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name': 'Hyperpay Payment Acquirer',
    'category': 'Hidden',
    'summary': 'Payment Acquirer: Hyperpay Implementation',
    'version': '1.0',
    'author': 'Fore-Vision',
    'description': """Hyperpay Payment Acquirer""",
    'depends': ['payment'],
    'data': [
        'views/hyperpay.xml',
        'views/payment_aquirer.xml',
        'data/hyperpay.xml',
        'views/payment_view.xml'
        
    ],
    'installable': True,
}
