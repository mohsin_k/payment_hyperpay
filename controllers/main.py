import json
import logging
import pprint
import werkzeug
import urllib
import urllib2

from openerp import http, SUPERUSER_ID
from openerp.http import request

from openerp.addons.payment.models.payment_acquirer import ValidationError



_logger = logging.getLogger(__name__)

class HyperpayController(http.Controller):

    _return_url = '/payment/hyperpay/return'

    @http.route([
        '/shop/payment/hyperpay',
    ], type='http', auth="public", website=True)
    def hyperpay(self, **post):
        cr, uid, context = request.cr, request.uid, request.context
        base_url = request.env['ir.config_parameter'].get_param('web.base.url')
        return_url = base_url  + '/payment/hyperpay/return'
        token = self.get_token()
        hyperpay_url = """
<!doctype html>
<html>
  <head>
    <script async src="https://test.ctpe.net/frontend/widget/v4/widget.js;jsessionid=%s"></script>
  </head>
  <body>
    <form action="%s" id="%s">VISA MASTER CHINAUNIONPAY SOFORTUEBERWEISUNG </form>
  </body>
</html>
"""%(token,return_url,token)
        return hyperpay_url

    @http.route([
        '/payment/hyperpay/return'
    ], type='http', auth='public', website=True)
    def hyperpay_feedback(self, **post):
        if post.get('token',False):
            cr, uid, context = request.cr, request.uid, request.context
            sale_order_id = request.session.get('sale_last_order_id')
            if sale_order_id:
                order = request.registry['sale.order'].browse(cr, SUPERUSER_ID, sale_order_id, context=context)
                return request.website.render("website_sale.confirmation", {'order': order})
            return request.website.render("payment_hyperpay.hyperpay_successfull")
        else:
            return request.website.render("payment_hyperpay.hyperpay_failure")

    def get_token(self,**post):
        cr, uid, context = request.cr, request.uid, request.context
        
        payment_aquirer = request.env['payment.acquirer'].search([('provider','=','hyperpay')])
        request.context.update({'aquirer_id': payment_aquirer.id})
        sender_id = payment_aquirer.hyperpay_sender_id
        channel_id = payment_aquirer.hyperpay_channel_id
        user_login = payment_aquirer.hyperpay_user_login
        user_pwd = payment_aquirer.hyperpay_user_pwd
        
        order = request.website.sale_get_order(force_create=1, context=context)
	data = {
    		"SECURITY.SENDER": sender_id,
    		"TRANSACTION.CHANNEL": channel_id,
    		"TRANSACTION.MODE": "INTEGRATOR_TEST",
    		"USER.LOGIN": user_login,
    		"USER.PWD": user_pwd,
    		"PAYMENT.TYPE": "DB",
    		"PRESENTATION.AMOUNT": order.amount_total,
    		"PRESENTATION.CURRENCY": order.pricelist_id.currency_id.name
		}
	try:
            response = urllib2.urlopen('https://test.ctpe.net/frontend/GenerateToken', urllib.urlencode(data) )
            if response.code == 200:
                result = json.loads(response.read())
                if 'transaction' in result:
                    return result['transaction']['token']
                
	except urllib2.HTTPError, e:
            raise ValidationError('HTTPError = ' + str(e.reason))
        except urllib2.URLError, e:
            raise ValidationError('URLError = ' + str(e.reason))
        except Exception, e:
            raise ValidationError(str(e))
        return True
        
