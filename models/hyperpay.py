##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import models, fields, api

class PaymentAcquirerHyperpay(models.Model):
    _inherit = 'payment.acquirer'

    def _get_providers(self, cr, uid, context=None):
        providers = super(PaymentAcquirerHyperpay, self)._get_providers(cr, uid, context=context)
        providers.append(['hyperpay', 'Hyperpay'])
        return providers



    hyperpay_sender_id = fields.Char('Sender Id', required_if_provider='hyperpay')
    hyperpay_channel_id = fields.Char('Channel Id', required_if_provider='hyperpay')
    hyperpay_user_login = fields.Char('User Login',required_if_provider='hyperpay')
    hyperpay_user_pwd = fields.Char('User Password',required_if_provider='hyperpay')


        
        